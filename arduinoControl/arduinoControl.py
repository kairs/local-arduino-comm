import socket
import sys
import threading
import select
import time

ARDUINO_IP="192.168.1.177"
ARDUINO_PORT=8888

LOCAL_IP="192.168.1.16"
LOCAL_PORT=8888

waiting_status = False
computer_status = "NOSTATUS"
status_valid = False

waiting_ack = False
ack_content = ""
ack_content_valid = False

TIMEOUT_REQUEST=10

def send_udp_packet(packet):
	sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
	sock.sendto(bytes(packet, "utf-8"), (ARDUINO_IP, ARDUINO_PORT))

## Must be run on other thread than main 
def udp_reception():
	sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
	sock.bind((LOCAL_IP, LOCAL_PORT))
	sock.setblocking(0)
	sock.settimeout(3)
	global waiting_status
	global computer_status
	global status_valid

	global waiting_ack
	global ack_content
	global ack_content_valid
	while True:
		try:
			data, addr = sock.recvfrom(1024)
			if (addr[0] == ARDUINO_IP and addr[1] == ARDUINO_PORT):
				if (data and waiting_status):
					decoded_data = data.decode('UTF-8').split(":")
					if decoded_data[0] == "STATUS":
						status_valid = True
						computer_status = decoded_data[1]
					else:
						status_valid = False
						computer_status = "NOSTATUS"
				if (data and waiting_ack):
					decoded_data = data.decode('UTF-8').split(":")
					if decoded_data[0] == "ACK":
						ack_content_valid = True
						ack_content = decoded_data[1]
					else:
						ack_content_valid = False

		except:
			pass


def getStatus():
	global waiting_status
	global computer_status
	global status_valid
	waiting_status = True
	status_valid = False
	ts = time.time()
	send_udp_packet ("STATUS\0")
	while waiting_status:
		if status_valid:
			print (computer_status)
			waiting_status = False
		if (time.time() > ts + TIMEOUT_REQUEST):
			print ("Timeout on getStatus")
			waiting_status = False
	

def launchCommand(command):
	global waiting_ack
	global ack_content
	global ack_content_valid
	waiting_ack = True
	ack_content_valid = False
	ts = time.time()
	send_udp_packet (command + "\0")
	while waiting_ack:
		if ack_content_valid and ack_content == command:
			print ("Received ack for command " + command)
			waiting_ack = False
		if (time.time() > ts + TIMEOUT_REQUEST):
			print ("Time out on ack reception ")
			waiting_ack = False


def print_usage():
	print ("Usage: arduinoControl.py <COMMAND>")
	print ("Where command can be")
	print (" - BOOT")
	print (" - REBOOT")
	print (" - STATUS")
	print (" - SHUTDOWN")
	print (" - HARD_SHUTDOWN")
	print (" - SHELL (interactive shell)")


def launch_shell():
	shell_open=True
	while (shell_open):
		str_query = input (">> ").lower()
		if (str_query == "exit"):
			shell_open=False
		elif (str_query == "status"):
			getStatus()
		elif (str_query == "boot"):
			launchCommand("BOOT")
		elif (str_query == "boot"):
			launchCommand("BOOT")
		elif (str_query == "reboot"):
			launchCommand("REBOOT")
		elif (str_query == "shutdown"):
			launchCommand("SHUTDOWN")
		elif (str_query == "hard_shutdown"):
			launchCommand("HARD_SHUTDOWN")
		elif (str_query == "help"):
			print_usage()
		else:
			print ("Unkown command (type exit to leave)")



def main():
	argc=len(sys.argv) - 1

	udp_thread= threading.Thread (target=udp_reception)
	udp_thread.daemon = True
	udp_thread.start()

	if argc == 1:
		argument = sys.argv[1].upper()
		if argument == "BOOT":
			launchCommand("BOOT")
		elif argument == "REBOOT":
			launchCommand("REBOOT")
		elif argument == "STATUS":
			getStatus()
		elif argument == "SHUTDOWN":
		    launchCommand("SHUTDOWN")
		elif argument == "HARD_SHUTDOWN":
		    launchCommand("HARD_SHUTDOWN")
		elif argument == "SHELL":
			print ("Launching shell control")
			launch_shell()
		else:
			print_usage()
	else:
		print_usage()


if __name__ == "__main__":
	main()
