#include <Arduino.h>
#include <SPI.h>          // needed for Arduino versions later than 0018
#include <Ethernet.h>
#include <EthernetUdp.h>  // UDP library from: bjoern@cs.stanford.edu 12/30/2008
#include <SoftReset.h>

using namespace std;


/**
 * ============================
 * == ETHERNET CONFIGURATION ==
 * ============================
 */
byte mac[] = {  0x00, 0x01, 0x02, 0xAA, 0xBB, 0xCC };

IPAddress ip(192, 168, 1, 177);
unsigned int localPort = 8888;

EthernetUDP Udp;

IPAddress remoteIp(192, 168, 1, 16);
unsigned int remotePort = 8888;


/**
 * ===============================
 * == PIN CONTROL CONFIGURATION ==
 * ===============================
 */

const int powerControlPin = 3;
const int resetControlPin = 2;

const int powerAnalogPin = A3;
const int tempAnalogPin = A0;
const double resolutionArduino = 0.0049;

const int cyclesMax = 5000; 
int counterCycle = 0;

int currentTemp = 0;
const int maxTemp = 50;

void lightPin(int pin, unsigned long duration) {
  digitalWrite (pin, HIGH);
  delay (duration);
  digitalWrite (pin, LOW);
}

enum COMMAND {
  BOOT,
  REBOOT,
  SHUTDOWN,
  HARD_SHUTDOWN,
  STATUS,
  UNKNOWN
};

int udpPacketPresence() {
  return Udp.parsePacket();
}

COMMAND identifyCommand(String cmd) {
  Serial.print ("Identifying command ");
  Serial.println (cmd);
  if (cmd.compareTo ("BOOT") == 0) {
    return BOOT;
  } else if (cmd.compareTo ("REBOOT") == 0) {
    return REBOOT;
  } else if (cmd.compareTo ("SHUTDOWN") == 0) {
    return SHUTDOWN;
  } else if (cmd.compareTo ("HARD_SHUTDOWN") == 0) {
    return HARD_SHUTDOWN;
  } else if (cmd.compareTo ("STATUS") == 0) {
    return STATUS;
  } else {
    return UNKNOWN;
  }
}

void sendUdpPacket(IPAddress ip, uint16_t port, String content) {
  Udp.beginPacket(ip, port);
  char buffer[content.length() + 1]; 
  strcpy(buffer, content.c_str()); 
  Udp.write(buffer);
  Udp.endPacket();
}

double readTemp() {
  int valueTempPin = analogRead(tempAnalogPin);
  double voltValueTemp = resolutionArduino * (double) valueTempPin;
  double temp = (voltValueTemp - 0.5) * 100;
  double experimentalOffset = 3.5;
  temp -= experimentalOffset;
  return temp;
}

void hardShutdown() {
  Serial.println ("HARD_SHUTDOWN");
  lightPin (powerControlPin, 5000);
  sendUdpPacket(remoteIp, remotePort, "ACK:HARD_SHUTDOWN");
}


void processUdpPacket() {
  Serial.println("Received packet");

  char incomingBuffer[UDP_TX_PACKET_MAX_SIZE];
  Udp.read(incomingBuffer, UDP_TX_PACKET_MAX_SIZE);

  String command (incomingBuffer);
  switch (identifyCommand(command)) {
  case BOOT:
  {
    Serial.println ("BOOT command identified");
    lightPin (powerControlPin, 500);
    sendUdpPacket(remoteIp, remotePort, "ACK:BOOT");
    break;
  }

  case REBOOT:
  {
    Serial.println ("REBOOT command identified");
    lightPin (resetControlPin, 500);
    sendUdpPacket(remoteIp, remotePort, "ACK:REBOOT");
    break;
  }
  case SHUTDOWN:
  {
    Serial.println ("SHUTDOWN command identified");
    lightPin (powerControlPin, 500);
    sendUdpPacket(remoteIp, remotePort, "ACK:SHUTDOWN");
    break;
  }
  case HARD_SHUTDOWN:
  {
    hardShutdown();
    break;
  }
  case STATUS:
  {
    Serial.println ("Status command identified");
    int valuePwr = analogRead(powerAnalogPin);
    double voltValuePwr = resolutionArduino * (double) valuePwr;
    double temp = readTemp();
    char bufferTmp[32];
    dtostrf(temp,5,2,bufferTmp);

    if (voltValuePwr > 3) {
      Serial.println ("Sending Status On");
      String message =  "STATUS:ON;TEMP=";
      message += bufferTmp;
      sendUdpPacket (remoteIp, remotePort, message);
    } else {
      Serial.println ("Sending Status Off");
      String message =  "STATUS:OFF;TEMP=";
      message += bufferTmp;
      sendUdpPacket (remoteIp, remotePort, message);
    }
    break;
  }
  case UNKNOWN:
  {
    sendUdpPacket(remoteIp, remotePort, "UNKNOWN COMMAND");
    Serial.println ("No command identified");
    break;
  }
  }
}


/**
 * ============================
 * == ARDUINO COMMON METHODS ==
 * ============================
 */

void setup() {
  pinMode(resetControlPin, OUTPUT);
  pinMode(powerControlPin, OUTPUT);

  digitalWrite(resetControlPin, LOW);
  digitalWrite(powerControlPin, LOW);

  Ethernet.begin(mac, ip);
  Udp.begin(localPort);

  Serial.begin(9600);
}


void loop() {

  if (udpPacketPresence()) {
    processUdpPacket();
  }

  currentTemp = readTemp();

  if (currentTemp > maxTemp) {
    hardShutdown();
  }

  if (counterCycle == cyclesMax) {
    soft_restart();
  }
  counterCycle++;

  delay(1000);
}