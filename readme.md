
This project contains two parts.

# ARDUINO SERVER

This first part has an objective to control the life of a computer through a direct connection to its ATX pins.
It is able to receive the following commands and act accordingly:
 - BOOT
 - SHUTDOWN
 - REBOOT

Moreover, it is linked to a temperaure monitor. In the event of the temperature raising above a prefixec threshold, it will perform a shutdown of the computer.
 
Commands are sent to the arduino throuh udp, on a local network. The arduino acknowledges those commands.

## Motherboard interface
The arduino is connected to the PINs of a motherboard ASUS P5PKL-AM:
![alt text](doc/P5KPL-AM-pins.png "Motherboard pins")

The arduino pins are connected to the ATX pins as described hearefter:
![alt text](doc/atx_arduino_connection.png "Arduino pins connection")

## Temperature controller interface

TODO once temp sensors are received  

# ARDUINO CONTROL

This second part is a python interface for controlling the arduino (and through the arduino the distant computer). It creates, send and control acknowledgment of commands to the arduino through a minimal python program.
The arduino control is mean to be used on a computer on the same local network.

The following commands are available:
- BOOT
- REBOOT
- STATUS
- SHUTDOWN
- HARD SHUTDOWN





